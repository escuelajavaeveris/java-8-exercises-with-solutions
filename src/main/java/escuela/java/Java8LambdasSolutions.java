package escuela.java;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Created by hgranjal on 12/06/2018.
 */
public class Java8LambdasSolutions {

    //TODO: hablar de pasar por referencia o valor
    protected static void sortStudentsByFirstName(List<Person> people) {
        Collections.sort(people, (p1, p2) -> p1.getFirstName().compareTo(p2.getFirstName()));
    }

    public static List<Person> filterPeopleBetween20And25(List<Person> people) {
        List<Person> filteredPeople = filterPeople(people, p -> p.getAge().compareTo(20) >= 0 && p.getAge().compareTo(25) <= 0);
        return filteredPeople;
    }

    public static List<Person> filterPeopleWithNameStartedWithP(List<Person> people) {
        List<Person> filteredPeople = filterPeople(people, p -> p.getFirstName().startsWith("P"));
        return filteredPeople;
    }

    public static List<Person> filterPeople(List<Person> people, Predicate<Person> condition) {
        List<Person> filteredPeople = new ArrayList<>();
        for (Person p : people) {
            if (condition.test(p)) {
                filteredPeople.add(p);
            }
        }
        return filteredPeople;
    }
}
