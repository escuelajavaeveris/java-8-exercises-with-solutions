package escuela.java;

import java.util.Optional;

/**
 * Created by hgranjal on 13/06/2018.
 */
public class Java8OptionalsSolutions {

    public static String getStreetFromPerson(Person2 person) {
        return Optional.ofNullable(person)
                .map(Person2::getAddress)
                .map(Address::getStreet)
                .orElse("unknown");
    }
}
