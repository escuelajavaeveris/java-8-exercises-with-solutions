package escuela.java;

/**
 * Created by hgranjal on 13/06/2018.
 */
public class Address {
    private String street;
    private Integer door;
    private Integer floor;
    private String postalCode;

    public Address(String street, Integer door, Integer floor, String postalCode) {
        this.street = street;
        this.door = door;
        this.floor = floor;
        this.postalCode = postalCode;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public Integer getDoor() {
        return door;
    }

    public void setDoor(Integer door) {
        this.door = door;
    }

    public Integer getFloor() {
        return floor;
    }

    public void setFloor(Integer floor) {
        this.floor = floor;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }
}
