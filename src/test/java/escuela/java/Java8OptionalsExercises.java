package escuela.java;

import org.junit.Test;
import org.testng.Assert;

import java.util.Optional;

/**
 * Created by hgranjal on 13/06/2018.
 */
public class Java8OptionalsExercises {

    @Test
    public void testNullPointerNotThrown() {
        Person2 teacher = new Person2("Henrique", "Lima", 35, null);
        String street = Java8OptionalsSolutions.getStreetFromPerson(teacher);
        Assert.assertEquals(street, "unknown");
    }
}
