package escuela.java;

import org.hamcrest.Condition;
import org.junit.Test;
import org.testng.Assert;

import java.util.Arrays;
import java.util.List;

/**
 * Created by hgranjal on 12/06/2018.
 */
public class Java8LambdasExercises {

    List<Person> students = Arrays.asList(
            new Person("Patrici", "Duenas", 30),
            new Person("Lorena", "Sandoval", 25),
            new Person("Oscar", "Lora", 36),
            new Person("Elena", "Rodriguez", 23),
            new Person("Ionela", "Teclea", 27)
    );

    @Test
    public void testSortPeopleByFirstName() {
        Java8LambdasSolutions.sortStudentsByFirstName(students);
        Assert.assertEquals(students.get(0).getFirstName(), "Elena");
        Assert.assertEquals(students.get(1).getFirstName(), "Ionela");
        Assert.assertEquals(students.get(2).getFirstName(), "Lorena");
        Assert.assertEquals(students.get(3).getFirstName(), "Oscar");
        Assert.assertEquals(students.get(4).getFirstName(), "Patrici");
    }

    @Test
    public void testFilterPeopleBetween20And25() {
        List<Person> filteredStudents = Java8LambdasSolutions.filterPeopleBetween20And25(students);
        Assert.assertEquals(filteredStudents.get(0).getFirstName(), "Lorena");
        Assert.assertEquals(filteredStudents.get(1).getFirstName(), "Elena");
    }

    @Test
    public void testFilterPeopleWithNameStartedWithP() {
        List<Person> filteredStudents = Java8LambdasSolutions.filterPeopleWithNameStartedWithP(students);
        Assert.assertEquals(filteredStudents.get(0).getFirstName(), "Patrici");
    }
}
