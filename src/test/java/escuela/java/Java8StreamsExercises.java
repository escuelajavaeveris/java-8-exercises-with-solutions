package escuela.java;

import org.junit.Test;
import org.testng.Assert;

import java.util.Arrays;
import java.util.List;

/**
 * Created by hgranjal on 12/06/2018.
 */
public class Java8StreamsExercises {

    List<Person> students = Arrays.asList(
            new Person("Patrici", "Duenas", 30),
            new Person("Lorena", "Sandoval", 25),
            new Person("Oscar", "Lora", 36),
            new Person("Elena", "Rodriguez", 23),
            new Person("Ionela", "Teclea", 27)
    );

    @Test
    public void testSortPeopleByFirstName() {
        List<Person> sortedStudents = Java8StreamsSolutions.sortStudentsByFirstName(students);
        Assert.assertEquals(sortedStudents.get(0).getFirstName(), "Elena");
        Assert.assertEquals(sortedStudents.get(1).getFirstName(), "Ionela");
        Assert.assertEquals(sortedStudents.get(2).getFirstName(), "Lorena");
        Assert.assertEquals(sortedStudents.get(3).getFirstName(), "Oscar");
        Assert.assertEquals(sortedStudents.get(4).getFirstName(), "Patrici");
    }

    @Test
    public void testFilterPeopleBetween20And25() {
        List<Person> filteredStudents = Java8StreamsSolutions.filterPeopleBetween20And25(students);
        Assert.assertEquals(filteredStudents.get(0).getFirstName(), "Lorena");
        Assert.assertEquals(filteredStudents.get(1).getFirstName(), "Elena");
    }

    @Test
    public void testFilterPeopleWithNameStartedWithP() {
        List<Person> filteredStudents = Java8StreamsSolutions.filterPeopleWithNameStartedWithP(students);
        Assert.assertEquals(filteredStudents.get(0).getFirstName(), "Patrici");
    }

    @Test
    public void testIsThereSomeoneOlderThan35() {
        boolean isOlder = Java8StreamsSolutions.isThereSomeoneOlderThan35(students);
        Assert.assertTrue(isOlder);
    }

    @Test
    public void testCalculateAgeAverage() {
        double ageAvg = Java8StreamsSolutions.calculateAgeAverage(students);
        Assert.assertEquals(ageAvg, 28.2d);
    }
}
